# my-video

## 2020最新款影视小程序后端+前端

### 该项目的部署需要具备微擎框架的开发以及部署能力

### 小程序demo展示
<div align=center>
    <img src="https://gitee.com/971664595/my-video/raw/master/image/feng.jpg" width="200" height="200" />
    <img src="https://gitee.com/971664595/my-video/raw/master/image/yun.jpg" width="200" height="200" />
</div>

### 前端界面展示
<div align=center>
    <img src="https://gitee.com/971664595/my-video/raw/master/image/q1.png" /><br>
    <img src="https://gitee.com/971664595/my-video/raw/master/image/q2.png" /><br>
    <img src="https://gitee.com/971664595/my-video/raw/master/image/q3.png" />
</div>

